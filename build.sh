#!/bin/sh
set -e #break on error

docker login --username yacineb --password $HELLO_TEAM_DOCKER_HUB_PASSWORD
VERSION="2.1.0-node-lts"

IMAGE_NAME="luckycrush/base-build:$VERSION"
docker build . -f Dockerfile.base_build -t $IMAGE_NAME
docker push $IMAGE_NAME

IMAGE_NAME="luckycrush/base-deploy:$VERSION"
docker build . -f Dockerfile.base_deploy -t $IMAGE_NAME
docker push $IMAGE_NAME
